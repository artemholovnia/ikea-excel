import os

APPS = [
    'auth',
    'core',
    'editor',
    'panel',
    'scraper'
]
SECRET_KEY = 'ix3m1dk2lwj_9x5mlm0z4ijgpyhdpntvktxn58cpfukyejmtz1vk0c_stmtrw448'
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STORAGE_PATH = os.path.join(BASE_DIR, 'storage')
STORAGE_LIMIT = 20

# Ids of stores to parse stock info
STORES = [
    (204, 'Краков'),
    (311, 'Люблин'),
    (188, 'Варшава (Janki)'),
    (307, 'Варшава (Targowek)'),
]

# Header for request which parses stock info
X_CLIENT_ID_HEADER = 'b6c117e5-ae61-4ef5-b4cc-e0b1e37f0631'
