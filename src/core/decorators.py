import functools
from typing import Callable, Type

from aiohttp.web_exceptions import HTTPSeeOther, HTTPException


def login_required(redirect_to:str=None, raise_http:Type[HTTPException]=None, is_method:bool=False) \
-> Callable:
    assert redirect_to or raise_http, 'redirest_to or raise_http are required'
    def wrapper(func):
        @functools.wraps(func)
        async def wrapped_func(request, *args, **kwargs):
            _request = request.request if is_method else request
            if _request.is_session_still_active:
                return await func(request, *args, **kwargs)
            return HTTPSeeOther(redirect_to) if redirect_to else raise_http
        return wrapped_func
    return wrapper