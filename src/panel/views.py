import os
from datetime import datetime

import aiohttp_jinja2
import pytz
from aiohttp import web
from aiohttp.web_exceptions import HTTPForbidden
from aiohttp.web_request import Request
from aiohttp.web_response import json_response, Response

from conf import settings
from src.core.decorators import login_required
from src.panel.functions import save_file, files_in_storage, parse_filename


@login_required(redirect_to='/login')
async def panel(request: Request):
  return aiohttp_jinja2.render_template('panel/panel.html', request, {})


@login_required(raise_http=HTTPForbidden)
async def list(request: Request):
    data = []
    for f in files_in_storage():
        original_filename, timestamp, processing = parse_filename(f)
        date = datetime.fromtimestamp(timestamp, tz=pytz.timezone('Europe/Kiev')).strftime('%Y-%m-%d %H:%M:%S')
        data.append({
            'date': date,
            'filename': original_filename,
            'download_url': request.app.router['serve'].url_for(filename=f).path if not processing else '',
            'delete_url': request.app.router['delete'].url_for(filename=f).path if not processing else ''
        })

    return json_response(data)


@login_required(raise_http=HTTPForbidden)
async def save(request: Request):
    post = await request.post()
    await save_file(post.get('file'))
    return web.json_response({})


@login_required(raise_http=HTTPForbidden)
async def delete(request: Request):
   filename = request.match_info['filename']
   file_path = os.path.join(settings.STORAGE_PATH, filename)
   if os.path.exists(file_path):
       os.remove(file_path)
       return web.HTTPOk()
   return web.HTTPNotFound()


@login_required(raise_http=HTTPForbidden)
async def serve(request: Request):
  filename = request.match_info['filename']
  with open(os.path.join(settings.STORAGE_PATH, filename), 'rb') as f:
     original_filename, *_ = parse_filename(filename)
     response = Response(content_type='application/octet-stream',
                         headers={'Content-Disposition': f'attachment; filename="{original_filename}"'}
                        )
     response.body = f.read()
     return response