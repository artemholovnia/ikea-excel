from aiohttp.hdrs import METH_GET, METH_DELETE, METH_POST
from aiohttp.web_routedef import route, get

from src.panel.views import serve, list, save, delete, panel

routes = [
    route(METH_GET, '/', panel, name='panel'),

    route(METH_GET, '/files', list),
    route(METH_POST, '/files', save),

    route(METH_DELETE, '/files/{filename}', delete, name='delete'),
    route(METH_GET, '/files/{filename}', serve, name='serve')
]

