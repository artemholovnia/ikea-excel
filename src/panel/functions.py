import asyncio
import datetime
import os

from aiohttp.web_request import FileField

from conf import settings
from src.core import queue
from src.editor.functions import read, write
from src.scraper.functions import get_product_info

def filename_by_state(original:str, state:str) -> str:
    return f'{int(datetime.datetime.utcnow().timestamp())}.{state}.{original}'

def parse_filename(filename:str) -> (str, datetime.datetime, int, bool):
    start_index, processing =  (2, True) if '.processing' in filename else (1, False)
    timestamp = filename.split('.')[0]
    return '.'.join(filename.split('.')[start_index:]), int(timestamp), processing

def files_in_storage() -> [str]:
    files = sorted(list(os.walk(settings.STORAGE_PATH))[0][-1], reverse=True)
    return files

async def save_file(file: FileField) -> None:
    actual_filename = filename_by_state(file.filename, 'processing')
    with open(os.path.join(settings.STORAGE_PATH, actual_filename), 'wb') as f:
        f.write(file.file.read())
    await queue.put(process_file(actual_filename))
    await asyncio.create_task(clean_storage())

async def clean_storage() -> None:
    files = files_in_storage()
    try:
        for f in files[settings.STORAGE_LIMIT:]:
            os.remove(os.path.join(settings.STORAGE_PATH, f))
    except IndexError:
        return

async def process_file(filename: str) -> None:
    source_file = os.path.join(settings.STORAGE_PATH, filename)
    wb, vendors = await read(source_file)

    result = {}
    counter, limiter = 0, 10
    max_retries = 3
    unsuccessful_history = {}
    while vendors[counter*limiter:counter*limiter+limiter]:
        partial_result = await asyncio.gather(*[get_product_info(i)
                                                for i in vendors[counter*limiter:counter*limiter+limiter]])
        result.update(dict(partial_result))
        unsuccessful = filter(lambda x: x[-1].get('unsuccessful', False), partial_result)
        for v, d in unsuccessful:
            if not (unsuccessful_history.get(v) and unsuccessful_history[v]['retries'] >= max_retries):
                try:
                    unsuccessful_history[v]['retries'] += 1
                except KeyError:
                    unsuccessful_history.setdefault(v, {'retries': 1})
                vendors.append(v)
        counter += 1

    wb = await write(wb, result)
    wb.save(source_file.replace('.processing', ''))
    os.remove(source_file)