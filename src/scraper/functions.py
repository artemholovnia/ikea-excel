import asyncio
import decimal
import json
import re
from contextlib import suppress
from operator import itemgetter
from typing import Tuple, Dict, Any

import aiohttp
import bs4
from aiohttp.hdrs import METH_GET

from conf import settings


def _normalize_vendor(vendor: str) -> str:
    return re.sub("[\s.\"\'s]+", "", vendor)


async def get_product_info(vendor: str) -> Tuple[str, Dict]:
    result = {}
    for res in await asyncio.gather(process_pl_info(vendor), process_ua_info(vendor)):
        result.update(res)
    return vendor, result


async def _process_ua_info_v2(vendor: str) -> Dict:
    result = {}
    url = f'https://sik.search.blue.cdtapps.com/ua/uk/search-result-page?q={vendor}&types=PRODUCT'
    async with aiohttp.client.request(METH_GET, url) as response1:
        _json = await response1.json()
        result['price_uah'] = decimal.Decimal(
            _json['searchResultPage']['products']['main']['items'][0]['product']['salesPrice']['numeral']
        ).quantize(decimal.Decimal('.00'))
    return result


async def process_ua_info(vendor: str) -> Dict:
    result = {}
    with suppress(Exception):
        result.update(await _process_ua_info_v2(_normalize_vendor(vendor)))
        assert not result
        result['unsuccessful'] = True

    return result


async def _process_pl_info_v2(vendor: str) -> Dict:
    result: Dict[str, Any] = {}
    url = f'https://sik.search.blue.cdtapps.com/pl/pl/search-result-page?q={vendor}&types=PRODUCT'
    async with aiohttp.client.request(METH_GET, url) as response1:
        _json = await response1.json()
        product_detail = _json['searchResultPage']['products']['main']['items'][0]['product']
        url = product_detail['pipUrl']

        # price
        quantize = lambda v: decimal.Decimal(v).quantize(decimal.Decimal(".00"))
        # якщо Ikea Family - берем попередню ціну без знижки
        if product_detail.get("tag") == "FAMILY_PRICE":
            result['price_pln'] = quantize(product_detail['salesPrice']["previous"]["wholeNumber"].replace(" ", ""))
        # в іншому випадку беремо актуальну ціну (навіть якщо є інша знижка ніж Ikea Family)
        else:
            result['price_pln'] = quantize(product_detail["salesPrice"]["numeral"])

        # weight
        async with aiohttp.client.request(METH_GET, url) as response2:
            _page = await response2.text()
            result['summary_weight'] = await calculate_summary_weight(_page)

        # stocks
        stocks = await get_stocks(vendor)
        for store_id, stock in stocks.items():
            result.setdefault('stocks', {})[store_id] = stock

    return result


async def process_pl_info(vendor: str) -> Dict:
    result = {}
    with suppress(Exception):
        result.update(await _process_pl_info_v2(_normalize_vendor(vendor)))
        assert not result
        result['unsuccessful'] = True

    return result


async def calculate_summary_weight(page: str) -> decimal.Decimal:
    page = bs4.BeautifulSoup(page)
    _json = json.loads(page.find('div', {'class':'js-product-information-section'}).get('data-initial-props'))
    packages = _json['dimensionProps']['packaging']['contentProps']['packages']
    packages = list(filter(lambda x: x['measurements'], packages))
    summary_weight = decimal.Decimal('0')
    for p in packages:
        quantity = p['quantity']['value']
        for m in p['measurements']:
            weight = list(filter(lambda x: x['label'] == 'Waga', m))[0]['value']
            summary_weight += quantity * decimal.Decimal(weight)
    return summary_weight.quantize(decimal.Decimal(".00"))


async def get_stocks(vendor: str) -> Dict:
    url = (
        'https://api.ingka.ikea.com/cia/availabilities/ru/pl?itemNos='
        f'{vendor}&expand=StoresList,Restocks,SalesLocations'
    )
    async with aiohttp.client.request(METH_GET, url, headers={'x-client-id': settings.X_CLIENT_ID_HEADER}) as response:
        _json = await response.json()
        stocks = {}
        for s in _json.get('data', []):
            with suppress(AssertionError, KeyError):
                assert s['classUnitKey']['classUnitCode'].isdigit()
                store_id = int(s['classUnitKey']['classUnitCode'])
                assert store_id in map(itemgetter(0), settings.STORES)
                stocks[store_id] = s['availableStocks'][0]['quantity']
        return stocks
