from aiohttp.web_routedef import route

from src.auth.views import login, logout

routes = [
    route('*', '/login', login, name='login'),
    route('*', '/logout', logout)
]