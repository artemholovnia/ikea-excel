import datetime
import string

import hashlib
import random
from sqlalchemy.exc import MultipleResultsFound, NoResultFound
from typing import Union

from conf import settings
from src.auth.schema import User

__salt = settings.SECRET_KEY


def make_password(password: str) -> str:
    hash = hashlib.sha256()
    hash.update(password.encode())
    hash.update(__salt.encode())
    return hash.hexdigest()

def create_session() -> str:
    return ''.join([random.choice(string.ascii_letters + string.digits) for i in range(32)])


def identify(username: str) -> Union[User, None]:
    from app import db

    try:
        return db.query(User).filter(User.username==username).one()
    except (MultipleResultsFound, NoResultFound):
        return


def authenticate(user: User, password: str) -> bool:
    return make_password(password) == user.password


def authorize(user: User) -> User:
    from app import db

    user.expired = datetime.datetime.utcnow() + datetime.timedelta(days=10)
    user.session = create_session()
    db.commit()
    return user


def forget(user: User) -> None:
    from app import db

    user.session = None
    user.expired = None
    db.commit()


def is_session_still_active(session: str):
    from app import db

    try:
        assert session
        user = db.query(User).filter(User.session==session, User.expired > datetime.datetime.utcnow()).one()
        return True, user
    except (MultipleResultsFound, NoResultFound, AssertionError):
        return False, None