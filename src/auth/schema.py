from sqlalchemy import Column, Integer, String, DateTime, MetaData
from sqlalchemy.orm import declarative_base

metadata = MetaData()
Base = declarative_base(metadata=metadata)

class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String(30), unique=True)
    password = Column(String(64))
    expired = Column(DateTime())
    session = Column(String(256))
