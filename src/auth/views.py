import aiohttp_jinja2
from aiohttp.hdrs import METH_POST
from aiohttp.web_exceptions import HTTPSeeOther

from src.auth.functions import forget, identify, authenticate, authorize
from src.core.decorators import login_required


async def login(request):
    if request.is_session_still_active:
        return HTTPSeeOther(request.app.router['panel'].url_for().path)
    if request.method == METH_POST:
        try:
            data = await request.post()
            assert data.get('username') and data.get('password')
            user = identify(data['username'])
            assert user
            valid = authenticate(user, data['password'])
            assert valid
            authorize(user)
            return HTTPSeeOther(request.app.router['panel'].url_for().path,
                                headers={'Set-Cookie':f'session={user.session}'})
        except AssertionError:
            return HTTPSeeOther(request.app.router['login'].url_for().path)
    return aiohttp_jinja2.render_template('auth/login.html', request, {})

@login_required(redirect_to='/login')
async def logout(request):
    forget(request.user)
    return HTTPSeeOther('/login')