from aiohttp.web_middlewares import middleware

@middleware
async def auth_middleware(request, handler):
    from src.auth.functions import is_session_still_active
    request.is_session_still_active, request.user = is_session_still_active(request.cookies.get('session'))
    return await handler(request)

async def user_context_middleware(request):
    return {'user': request.user}