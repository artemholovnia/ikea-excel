import sys
from sqlalchemy.exc import NoResultFound

from app import db
from src.auth.functions import make_password
from src.auth.schema import User

def create_user(username, password, update_password=False):
    try:
        user = db.query(User).filter(User.username==username).one()
        assert update_password, f'User "{username}" already exists. Use --update_password' \
                                f' to update password or create user with other username'
        user.password = make_password(password)
    except NoResultFound:
        db.add(User(username=username,
                    password=make_password(password)))
    finally:
        db.commit()