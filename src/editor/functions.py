import re

import openpyxl

from conf import settings

vendor_col = 2
weight_col = 4
pln_price_col = 5
uah_price_col = 6
stocks_start_col = 7

def normalize_vendor(vendor) -> str:
    vendor = str(vendor)
    return re.sub('^s', '', vendor.replace('.', ''))

async def read(filepath: str) -> (openpyxl.workbook.Workbook, []):
    wb = openpyxl.load_workbook(filepath)
    vendors = {normalize_vendor(v[vendor_col-1].value) for v in wb.active.rows if v[vendor_col-1].value}
    return wb, list(vendors)

async def write(wb: openpyxl.workbook.Workbook, data: {}) -> openpyxl.workbook.Workbook:
    for r in wb.active.rows:
        if d := data.get(normalize_vendor(r[vendor_col-1].value)):
            wb.active.cell(row=r[0].row, column=weight_col+1, value=d.get('summary_weight', ''))
            wb.active.cell(row=r[0].row, column=pln_price_col+1, value=d.get('price_pln', ''))
            wb.active.cell(row=r[0].row, column=uah_price_col+1, value=d.get('price_uah', ''))
            if stocks := d.get('stocks'):
                for i, (store_id, label) in enumerate(settings.STORES):
                    if stocks.get(store_id, None) is not None:
                        wb.active.cell(row=r[0].row, column=stocks_start_col+1+i, value=stocks[store_id])

    return wb