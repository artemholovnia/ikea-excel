import os

settings_module = os.getenv('APP_SETTINGS_MODULE', 'settings.dev')
variables = [
    'BASE_DIR',
    'DEBUG',
    'STORAGE_PATH',
    'STORES',
    'X_CLIENT_ID_HEADER'
]
settings = __import__(settings_module, fromlist=variables)