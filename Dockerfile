FROM python:3.9.6-alpine3.14
RUN apk add g++ bash
COPY requirements.txt /
RUN pip install -r /requirements.txt