new Vue({
  el: '#panel',
  delimiters: ['${', '}'],
  data() {
    return {
      files: [],
      sessionExpired: false
    };
  },
  mounted() {
    this.refresh();
    setInterval(this.refresh, 5000)
  },
  methods: {
    refresh: function() {
      axios.get('/files')
          .then(response => (this.files = response.data))
          .catch(error => {
            this.files = [];
            this.sessionExpired = true;
          })
    },

    deleteFile: function(url) {
      axios.delete(url)
          .then(response => (this.refresh()))
          .catch(error => {console.log('Ошибка удаления файла')})
    }
  }
});