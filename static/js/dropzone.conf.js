Dropzone.options.dropzone = {
  paramName: "file",
  maxFiles: 20,
  maxFilesize: 1,
  acceptedFiles: '.xlsx',
  dictDefaultMessage: 'Перетяни сюда файлы или кликни на область',
  dictFallbackMessage: 'Браузер не поддерживается',
  dictFileTooBig: 'Файл слишком большой, максимальный допустимый размер файла {{filesize}} МБ',
  dictInvalidFileType: 'Файл не поддерживается',
  dictResponseError: 'Произошла ошибка',
  dictMaxFilesExceeded: 'Добавлено максимально допустимое количество файлов'
};