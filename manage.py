import re
from importlib import import_module

import sys

from conf import settings

class CommandDoesNotExist(Exception): pass

if __name__ == '__main__':
    func, argv = sys.argv[1], sys.argv[2:]
    args = list(filter(lambda x: not x.startswith('-'), argv))
    kwargs = {}
    for k in filter(lambda x: x.startswith('-'), argv):
        try:
            key, val = k.split('=')
        except ValueError:
            key, val = k.split('=')[0], True

        assert re.match('-(-)?[a-z_]+', key)
        key = key[2:] if len(key) >= 2 and key[1] == '-' else key[1:]
        kwargs[key] = val

    for app in settings.APPS:
        try:
            m = import_module(f'src.{app}.commands')
            if hasattr(m, func):
                getattr(m, func)(*args, **kwargs)
                break
        except ModuleNotFoundError:
            pass
    else:
        raise CommandDoesNotExist(f'Command "{func}" does not exist')