import argparse
import asyncio
import os

import aiohttp_jinja2
import jinja2
from aiohttp import web
from aiohttp.web import Application
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from src.core.worker import worker

from conf import settings
from src.auth.middlewares import auth_middleware, user_context_middleware
from src.auth.routes import routes as auth_routes
from src.auth.schema import metadata as user_metadata
from src.panel.routes import routes as panel_routes

app = Application(middlewares=[auth_middleware])
db = Session(create_engine('sqlite:///db.sqlite3'))

def setup_external_libraries(app: web.Application) -> None:
   aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(os.path.join('src', 'templates')),
                        context_processors=[user_context_middleware])

def setup_routes(app: web.Application) -> None:
   app.router.add_routes(panel_routes)
   app.router.add_routes(auth_routes)

   if settings.DEBUG:
      app.router.add_static('/static', 'static')

def configure_db() -> None:
   user_metadata.create_all(db.bind)

parser = argparse.ArgumentParser()
parser.add_argument('--port')

if __name__ == "__main__":
   setup_external_libraries(app)
   setup_routes(app)
   configure_db()
   os.makedirs(settings.STORAGE_PATH, exist_ok=True)

   loop = asyncio.get_event_loop()
   loop.create_task(worker())

   args = parser.parse_args()
   web.run_app(app, port=args.port)
